package main

type Database struct {
	Host 		string 	`json:"host"`
	Port 		int 	`json:"port"`
	Name 		string 	`json:"name"`
	Login 		string 	`json:"login"`
	Password 	string 	`json:"password"`
}

type TelegramBot struct {
	API			string `json:"telegram_bot_api_key"`
}
