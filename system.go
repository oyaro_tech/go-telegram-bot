package main

import (
	"os"
	"fmt"
	"log"
	"strings"
	"io/ioutil"
	"database/sql"
	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-telegram-bot-api/telegram-bot-api"
)

func LoadConfig() (*Database, *tgbotapi.BotAPI) {
	fp, err := os.Open("config.json")
	if err != nil {
		fmt.Fprintf(os.Stderr, "[!] Помилка обробки файла з конфігураціями.\n%s\n", err)
		os.Exit(2)
	}
	defer fp.Close()

	r, err := ioutil.ReadAll(fp)
	db := new(Database)
	json.Unmarshal(r, db)

	tg_bot := new(TelegramBot)
	json.Unmarshal(r, &tg_bot)
	bot, err := tgbotapi.NewBotAPI(tg_bot.API)
	if err != nil {
		log.Panic(err)
	}

	return db, bot
}

func RequestToDatabase(db *Database, query string) [][]string {
	conn := ConnectToDatabase(db)
	rows, err := conn.Query(query)
	defer rows.Close()

	if err != nil {
		fmt.Fprintf(os.Stderr, "[!] Помилка запиту:\n%s\n", err)
	}

	var result [][]string
	cols, _ := rows.Columns()
	pointers := make([]interface{}, len(cols))
	container := make([]string, len(cols))

	for i, _ := range pointers {
		pointers[i] = &container[i]
	}

	for rows.Next() {
		rows.Scan(pointers...)
		result = append(result, container)
	}

	return result
}

func ConnectToDatabase(db *Database) *sql.DB {
	conn, err := sql.Open(
		"mysql",
		fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", db.Login, db.Password, db.Host, db.Port, db.Name),
	)

	if err != nil {
		fmt.Fprintf(os.Stderr, "[!] Помилка підключення до бази даних:\n%s\n", err)
		return nil
	}

	return conn
}

func ShowMembers(update tgbotapi.Update, db *Database, bot *tgbotapi.BotAPI) {
	if (update.Message.Chat.ID == 409703645) {
		conn := ConnectToDatabase(db)
		rows, _ := conn.Query("SELECT member_id, username, phone FROM members;")
		defer rows.Close()

		var members string
		var username string
		var phone string
		var member_id string
		var result []string

		for rows.Next() {
			err := rows.Scan(&member_id, &username, &phone)
			if err != nil {
				log.Fatal(err)
			}

			username = "@" + username
			phone = phone + "\n"
			result = append(result, member_id, username, phone)
		}

		members = " "

		for _, member := range result {
			members += strings.Trim(fmt.Sprint(member), "[]") + " "
		}

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, fmt.Sprintf("Зареєстровані користувачі:\n%s", members))
		log.Printf("[System] Відповідь для %s:\n%s", bot.Self.UserName, fmt.Sprintf("Зареєстровані користувачі:\n%s", members))
		msg.ReplyToMessageID = update.Message.MessageID
		bot.Send(msg)
	} else {
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Ви не адміністратор...")
		log.Printf("[System] Відповідь для %s: Ви не адміністратор...", bot.Self.UserName)
		msg.ReplyToMessageID = update.Message.MessageID
		bot.Send(msg)
	}
}

func FindMember(db *Database, chatID int64) bool {
	if (len(RequestToDatabase(db, fmt.Sprintf("SELECT member_id FROM members WHERE member_id=%d", chatID))) > 0) {
		log.Printf("[System] Користувач з ID %d вже є в базі даних.\n", chatID)
		return true
	} else {
		log.Printf("[System] Користувача з ID %d немає в базі даних.\n", chatID)
		return false
	}
}

func CreateMember (db *Database, username string, phone string, member_id int64 ) {
	RequestToDatabase(db, fmt.Sprintf("INSERT INTO members (username, phone, member_id) VALUES (\"%s\", \"%s\", \"%d\");", username, phone, member_id))
	log.Printf("[System] Користувач %s був успішно доданий в базу даних.\n", username)
}

func Equal (a string, b string) bool {
	return strings.EqualFold(a, b)
}
