package main

import (
	"fmt"
	"log"
	"github.com/go-telegram-bot-api/telegram-bot-api"
)

func main() {
	db, bot := LoadConfig()

	bot.Debug = false

	log.Printf("[System] Авторизовано як %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, _ := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil { // ignore any non-Message Updates
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		if (update.Message.IsCommand()) {
			switch update.Message.Command() {
			case "start":
				if (!FindMember(db, update.Message.Chat.ID)) {
					requestContactMessage := tgbotapi.NewMessage(update.Message.Chat.ID, "Для реєстрації в системі необхідний ваш номер")
					log.Printf("[System] Відповідь для %s: Для реєстрації в системі необхідний ваш номер\n", update.Message.From.UserName)
					acceptButton := tgbotapi.NewKeyboardButtonContact("Надіслати мої контакти")
					requestContactReplyKeyboard := tgbotapi.NewReplyKeyboard([]tgbotapi.KeyboardButton{acceptButton})
					requestContactMessage.ReplyMarkup = requestContactReplyKeyboard
					bot.Send(requestContactMessage)
				} else {
					msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Ви вже зареєстровані в системі.")
					log.Printf("[System] Відповідь для %s: Ви вже зареєстровані в системі.\n", update.Message.From.UserName)
					msg.ReplyToMessageID = update.Message.MessageID
					bot.Send(msg)
				}

			case "show_members":
				ShowMembers(update, db, bot)

			default:
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Я такої команди не знаю...")
				log.Printf("[System] Відповідь для %s: Я такої команди не знаю...\n", update.Message.From.UserName)
				msg.ReplyToMessageID = update.Message.MessageID
				bot.Send(msg)
			}
		} else {
			if (update.Message.Contact != nil) {
				log.Printf("[System] Користувач %s надіслав свої контактні дані.\n", update.Message.From.UserName)
				if (update.Message.Contact.UserID == update.Message.From.ID) {
					if (!FindMember(db, update.Message.Chat.ID)) {
						CreateMember(db, update.Message.From.UserName, update.Message.Contact.PhoneNumber, update.Message.Chat.ID)
						fmt.Println(RequestToDatabase(db, "SELECT * FROM members"))

						msg := tgbotapi.NewMessage(update.Message.Chat.ID, fmt.Sprintf("%s, дякую за реєстрацію!", update.Message.Contact.FirstName))
						log.Printf("[System] Відповідь для %s: %s дякую за реєстрацію!\n", update.Message.From.UserName, update.Message.Contact.FirstName)
						msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(false)  // Remove keyboard
						bot.Send(msg)
					}
				}
			}

			if (Equal(update.Message.Text, "привіт")) {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Хто вітається з ботом 😅")
				log.Printf("[System] Відповідь для %s: Хто вітається з ботом 😅\n", update.Message.From.UserName)
				msg.ReplyToMessageID = update.Message.MessageID
				bot.Send(msg)
			} else {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Я таке не знаю...")
				msg.ReplyToMessageID = update.Message.MessageID
				bot.Send(msg)
			}
		}
	}
}

